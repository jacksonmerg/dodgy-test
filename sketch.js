var px=400;
var py=375;
var pc=200;
var score=0;
var best=0;
var effect=255;
var diff=0;
var lives=3;
var screen=0;
var shake=0;
var enemies = [];
var projectiles = [];
var dead=false;
var screen=0;
var menu=1;
var skin=0;
var level=0;
var levels=1;
var screens=[title,levelselect,game,skins,game,gameover,gameover];
var name=["Default","Neo","Australlian","Spartan"]
var desc=["lol noob","There is no spoon","G'day mate!","This is Sparta!"];
var offset=[80,80,120,100]
var size=[160,160,200,240]
var sheet
function gameover() {
  background(200,0,0);
  textSize(60);
  fill(0);
  lives=3;
  score=0;
  effect=255;
  text("You Died",width/2-textWidth("You Died")/2,150);
  enemies = [];
  projectiles = [];
  var select=false;
  for(i=1;i<3;i++){
    
    if(abs(mouseY-(i*100+250))>30){
      fill(200);
      rect(40,i*100+220,width-80,60)
      textSize(30);
    }else{
      fill(200+menu*15);
      menu+=1/(menu*menu);
      select=true;
      rect(40-menu,i*100+220-menu,width-80+menu*2,60+menu*2)
      textSize(30+menu);
    }
    
    fill(0);
    text(["","Title Screen","Retry"][i],width/2-textWidth(["","Level Select","Retry"][i])/2,i*100+260);
  } 
  if(!select)menu=1;
}
function setup() {
  createCanvas(360, 640);
  sheet=loadImage("sheet.png")
}
function game(){
  if(skin==2){
    translate(width/2,height/2);
    rotate(PI);
    translate(-width/2,-height/2);
  }
  fill(255);
  background(0);
  textSize(100);
  text(level,width/2-textWidth(level)/2,height/2);
}
function skins(){
  
  background(0);
  textSize(60);
  if(skin<0)skin+=desc.length;
  if(skin>=desc.length)skin-=desc.length;
  fill(0,255,0)
  ellipse(180,height/3+offset[skin],160,160)
  image(sheet.get(skin*128,0,128,128),180-size[skin]/2,height/3-size[skin]/2+80,size[skin],size[skin]);
  fill(255);
  text(["Default","Neo","Australlian","Spartan"][skin],width/2-textWidth(["Default","Neo","Australlian","Spartan"][skin])/2,height/5);
  textSize(30);
  text('"'+desc[skin]+'"',width/2-textWidth(desc[skin])/2,height*3/4);
}
function title() {
  background(0);
  textSize(90);
  fill(255);
  
  text("Dodgy",width/2-130,150);
  
  var select=false;
  for(i=0;i<3;i++){
    
    if(abs(mouseY-(i*100+250))>30){
      fill(200);
      rect(40,i*100+220,width-80,60)
      textSize(30);
    }else{
      fill(200+menu*15);
      menu+=1/(menu*menu);
      select=true;
      rect(40-menu,i*100+220-menu,width-80+menu*2,60+menu*2)
      textSize(30+menu);
    }
    
    fill(0);
    text(["Level Select","Endless","Skins"][i],width/2-textWidth(["Level Select","Endless","Skins"][i])/2,i*100+260);
  } 
  if(!select)menu=1;
}
function levelselect() {
  background(0);
  textSize(50);
  fill(255);
  text("Level Select:",width/2-140,100);
  textSize(30);
  var select=false;
  for(i=0;i<width;i+=width/6)for(j=0;j<width;j+=width/6){
	var k=1+i*6/width+j*36/width
    if(k>levels){
      fill(100)
      rect(i+5,j+5+height/5,50,50)
    }
    if(k<=levels){fill(200)
    if(mouseX<i+55+menu&&mouseX>i+5&&mouseY<j+55+menu+height/5&&mouseY>j+5+height/5){
      fill(200+menu*15)
      rect(i+5-menu/2,j+5+height/5-menu/2,50+menu,50+menu)
      select=true;
      menu+=1/(menu*menu);
    }else{
      rect(i+5,j+5+height/5,50,50)
    }
    }
    fill(0);
    text(k,i+30-textWidth(k)/2,j+40+height/5)
  }
  if(!select)menu=1;
    fill(255)
    textSize(40);
    text("Back",width/2-170,400+height/5);}
function mousePressed(){
   if(screen==5||screen==6){ 
    if(abs(mouseY%100-50)<30&&mouseY<500){
      if(mouseY>300)screen=[[0,2],[0,4]][screen-5][Math.floor(mouseY/100)-3]  
      screen=constrain(screen,0,screens.length-1)
      if(screen==2)level=1;
    }
  }else if(screen==3){
    if(mouseX<width/4&&abs(mouseY-height/2)<100)skin--;
    if(mouseX>width*3/4&&abs(mouseY-height/2)<100)skin++;
    if(mouseY>height-100)screen=0;
  }
  
  else if(screen==0){ 
    if(abs(mouseY%100-50)<30&&mouseY<500){
      screen=Math.floor(mouseY/100)-1  
      screen=constrain(screen,0,screens.length-1)
      if(screen==2)level=1;
    }
  }
  else if(screen==1){
    level=Math.floor(mouseX/60)+6*Math.floor(mouseY/60)-11;
    if(level>0&&level<=levels){
		screen=4;
		
	}
	if(mouseY>360+height/5&&mouseX<150)screen=0; 
    
  }
 
}
function draw(){  
  screens[screen]();
}

function polygon(x, y, radius, npoints) {
  let angle = TWO_PI / npoints;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius;
    let sy = y + sin(a) * radius;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}

function Projectile(x,y,d){
  this.pos=createVector(x,y);
  this.vel=createVector(cos(d-PI),sin(d-PI)).mult(15+3*Math.sqrt(diff));
  this.d=d-PI;
  this.i=4;
  this.update=function(){
	  this.pos.add(this.vel);
	  push();
	  translate(this.pos.x,this.pos.y)
	  rotate(this.d);
	  rect(-10,-5,20,10)
	  pop();
	  this.i-=1;
  }
  this.hit=function(){
	for (let j = 0; j < enemies.length; j++) {
      if(this.i<0&&dist(this.pos.x,this.pos.y,enemies[j].pos.x,enemies[j].pos.y)<30){
        enemies.splice(j,1)
		this.vel.mult(-.8)
		this.d+=PI;
        score++;
		shake=10;
		return true;
      }
    }
    if(dist(this.pos.x,this.pos.y,px,py)<30&&this.vel.mag()>.5&&shake==0){
	  if(score>best)best=score;
	  lives--;
	  shake=30;
	  this.vel.mult(-2)
	  this.d+=PI;
	  return true;
    }	
	return false;
	  
  }
}
function Enemy(x,y,t){
  this.c = 300;
  this.type=t;
  this.d=0;
  this.pos = createVector(x,y);
  this.vel = createVector(0,0);
  this.update = function(){
    this.c-=2;
	if(this.type==-1){
		this.type=0;
		this.c=0;
	}
	if(this.type==2)this.c-=10;
	if(this.type==3)this.c+=1
	var speed=(15+5*Math.sqrt(diff))
    this.vel.mult(1-(speed/300));
    if(this.c<0){
      if(this.type==0||this.type==2)this.vel.sub(createVector(speed*(this.pos.x-px)/dist(px,py,this.pos.x,this.pos.y),speed*(this.pos.y-py)/dist(px,py,this.pos.x,this.pos.y)))
      if(this.type==2)this.vel.mult(.5)
	  if(this.type==1)projectiles.push(new Projectile(this.pos.x,this.pos.y,this.d))
	  if(this.type==3)enemies.push(new Enemy(this.pos.x,this.pos.y,-1))
	  this.c=random(200,400);
    }
    this.pos.add(this.vel);
    if(this.pos.x<0||this.pos.x>width*2||this.pos.y>height*2||this.pos.y<0){
      this.vel.mult(-1)
	  this.d+=PI;
    }
    for (let j = 0; j < enemies.length; j++) {
      if(dist(this.pos.x,this.pos.y,enemies[j].pos.x,enemies[j].pos.y)<20&&dist(this.pos.x,this.pos.y,enemies[j].pos.x,enemies[j].pos.y)>0&&this.vel.mag()>enemies[j].vel.mag()){
        enemies.splice(j,1)
		this.vel.mult(-.8)
		this.d+=PI;
        score++;
		shake=10;
      }
    }
    if(dist(this.pos.x,this.pos.y,px,py)<15&&this.vel.mag()>.5&&shake==0){
	  if(score>best)best=score;
	  lives--;
	  shake=30;
	  this.vel.mult(-2)
	  this.d+=PI;
      if(lives==0)score=diff*diff;
	  
    }
    fill(255,255-this.c*(3+diff),0);
	this.d+=Math.sqrt(diff+1)*max(255-this.c*(3+diff),0)/1000*(atan2(this.pos.y-py,this.pos.x-px)-this.d)
	push();
    translate(this.pos.x,this.pos.y);
	rotate(this.d+PI/2);
	if(this.type==0)triangle(-15,-15,15,-15,0,15)
	if(this.type==2)triangle(-10,-20,10,-20,0,20)
	if(this.type==1)rect(-15,-15,30,30);
	if(this.type==3)polygon(0,0,25,5);
	pop();
  }
}
function setup() {
  createCanvas(windowWidth, windowHeight);
}
function game() {
  if(screen==4&&diff>levels)levels=diff
  if(shake>0)shake--;
  translate(random(-shake,shake),random(-shake,shake))
  if(diff!=Math.floor(Math.sqrt(score))&&effect>255){
	enemies=[]  
	effect=0;
	lives=3;
	dead=false;
	
  }
  if(lives==0){
	  if(screen==2)screen=5;
	  if(screen==4)screen=6;
  }
  diff=Math.floor(Math.sqrt(score))
  if(pc<0&&effect>255){
    pc=random(1000,2000)/(diff+5);
    enemies.push(new Enemy(random(width*2),random(height*2),Math.floor(random(min(4,(Math.sqrt(diff)))))))
  }

  pc--;
  effect+=3;
  if(diff*diff==score)effect--;
  scale(.5);
  background(0);
  textSize(300)
  fill(80);
  fill(80);
  text((score-diff*diff)+"/"+((diff+1)*(diff+1)-diff*diff),width-textWidth((score-diff*diff)+"/"+((diff+1)*(diff+1)-diff*diff))/2,height)
  textSize(100)
  text("Level "+(diff+1),width-textWidth("Level "+(diff+1))/2,height*4/3)
  textSize(30);
  fill(0,255,0);
  for(let i=0;i<lives;i++) ellipse(i*80+90,50,60,60);
  for (let i = 0; i < enemies.length; i++) {
    enemies[i].update();
  }
  for (let i = 0; i < projectiles.length; i++) {
    projectiles[i].update();
	if(projectiles[i].hit())projectiles.splice(i,1)
  }
  fill(0,255,0)
  noStroke();
  ellipse(px,py,60,60)
  if(dist(mouseX*2,mouseY*2,px,py)>30){
	px+=20*(mouseX*2-px)/dist(mouseX*2,mouseY*2,px,py);
	py+=20*(mouseY*2-py)/dist(mouseX*2,mouseY*2,px,py);
  }
  px=constrain(px,0,width*2)
  py=constrain(py,0,height*2)
  noStroke();
  fill(255,255-effect);
  if(dead)fill(255,0,0,255-effect);
  ellipse(width,height,effect*8,effect*8);
}